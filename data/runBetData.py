# -*- coding: utf-8 -*-
from app.BinanceAPI import BinanceAPI
from app.authorization import api_key,api_secret
import os,json
binan = BinanceAPI(api_key,api_secret)
# linux
data_path = os.getcwd()+"/data/data.json"
# windows
# data_path = os.getcwd() + "\data\data.json"

class RunBetData:

    def _get_json_data(self):
        '''读取json文件'''
        tmp_json = {}
        with open(data_path, 'r') as f:
            tmp_json = json.load(f)
            f.close()
        return tmp_json


    def _modify_json_data(self,data):
        '''修改json文件'''
        with open(data_path, "w") as f:
            f.write(json.dumps(data, indent=4))
        f.close()

    def get_ratio_coefficient(self):
        '''获取倍率系数'''
        data_json = self._get_json_data()
        return data_json['config']['RatioCoefficient']

    ####------下面为输出函数--------####

    def get_buy_price(self):
        data_json = self._get_json_data()
        return data_json["runBet"]["next_buy_price"]


    def get_sell_price(self):
        data_json = self._get_json_data()
        return data_json["runBet"]["grid_sell_price"]

    def get_cointype(self):
        data_json = self._get_json_data()
        return data_json["config"]["cointype"]

    def get_quantity(self,exchange_method=True):
        '''
        :param exchange: True 代表买入，取买入的仓位 False：代表卖出，取卖出应该的仓位
        :return:
        '''

        data_json = self._get_json_data()
        cur_step = data_json["runBet"]["step"] if exchange_method else data_json["runBet"]["step"] - 1 # 买入与卖出操作对应的仓位不同
        quantity_arr = data_json["config"]["quantity"]

        quantity = None
        if cur_step < len(quantity_arr): # 当前仓位 > 设置的仓位 取最后一位
            quantity = quantity_arr[0] if cur_step == 0 else quantity_arr[cur_step]
        else:
            quantity = quantity_arr[-1]
        return quantity

    def get_step(self):
        data_json = self._get_json_data()
        return data_json["runBet"]["step"]
    
    def get_ratio_coefficient(self):
        '''获取倍率系数'''
        data_json = self._get_json_data()
        return data_json['config']['RatioCoefficient']
    
    def set_ratio(self,symbol):
        '''修改补仓止盈比率'''
        data_json = self._get_json_data()
        profit_ratio = self.get_profit_ratio()  # 当前止盈比率
        double_throw_ratio = self.get_double_throw_ratio() # 当前补仓比率
        ratio_24hr = binan.get_ticker_24hour(symbol) #
        index = abs(ratio_24hr)

        if abs(ratio_24hr) > self.get_ratio_coefficient() * 10 : # 这是单边走势情况 只改变一方的比率
            if ratio_24hr > 0 : # 单边上涨，补仓比率不变
                data_json['config']['profit_ratio'] = round(self.get_ratio_coefficient() * 6 + self.get_step(), 1) # 系数的5倍+仓位数的2倍
                data_json['config']['double_throw_ratio'] = round(self.get_ratio_coefficient()* 4 + self.get_step(),1) # 多头比率为空头一半的多1系数
            else: # 单边下跌
                data_json['config']['double_throw_ratio'] = round(self.get_ratio_coefficient() * 6 + self.get_step(), 1)
                data_json['config']['profit_ratio'] = round(self.get_ratio_coefficient() * 4 + self.get_step(), 1)

        else: # 系数内震荡行情
            data_json['config']['profit_ratio'] = round(3 + self.get_step() / 2,1)  # 行情回调 比率缩小为1/2
            data_json['config']['double_throw_ratio'] = round(3 + self.get_step() / 2, 1)

        self._modify_json_data(data_json)


    # 买入后，修改 补仓价格 和 网格平仓价格以及步数
    def modify_price(self, deal_price,step):
        print("开始修改补仓价和网格价")
        data_json = self._get_json_data()
        data_json["runBet"]["next_buy_price"] = round(deal_price * (1 - data_json["config"]["double_throw_ratio"] / 100), 2) # 保留2位小数
        data_json["runBet"]["grid_sell_price"] = round(deal_price * (1 + data_json["config"]["profit_ratio"] / 100), 2)
        data_json["runBet"]["step"] = step
        self._modify_json_data(data_json)
        print("修改后的补仓价格为:{double}。修改后的网格价格为:{grid}".format(double=data_json["runBet"]["next_buy_price"],
                                                           grid=data_json["runBet"]["grid_sell_price"]))



if __name__ == "__main__":
    instance = RunBetData()
    # print(instance.modify_price(8.87,instance.get_step()-1))
    print(instance.get_quantity(False))
